SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for t_u_user
-- ----------------------------
DROP TABLE IF EXISTS `t_u_user`;
CREATE TABLE `t_u_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `nick_name` varchar(255) DEFAULT NULL,
  `sex` int(1) DEFAULT NULL,
  `register_date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_u_user
-- ----------------------------
BEGIN;
INSERT INTO `t_u_user` VALUES (1, 'a1', '1ee04e0b1cb5af7367c80c22e42efd8b', '张三', 1, '2020-04-09 14:24:23');
INSERT INTO `t_u_user` VALUES (2, 'a2', '1ee04e0b1cb5af7367c80c22e42efd8b', '李四', 1, '2020-04-09 14:24:23');
COMMIT;

SET FOREIGN_KEY_CHECKS = 1;
