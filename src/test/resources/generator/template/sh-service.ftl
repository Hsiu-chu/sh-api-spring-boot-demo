package ${baseProjectPackage}.service;
import ${baseProjectPackage}.model.${modelNameUpperCamel};
import ${baseProjectPackage}.model.${modelNameUpperCamel}DetailInput;
import ${baseProjectPackage}.model.${modelNameUpperCamel}ListInput;
import ${basePackage}.core.Service;
import com.github.pagehelper.PageInfo;


/**
 * Created by ${author} on ${date}.
 */
public interface ${modelNameUpperCamel}Service extends Service<${modelNameUpperCamel}> {
   public ${modelNameUpperCamel} findDetailByInput(${modelNameUpperCamel}DetailInput input);
   public PageInfo findListByInput(${modelNameUpperCamel}ListInput input);
}
