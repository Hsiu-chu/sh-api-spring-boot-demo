package ${baseProjectPackage}.service.impl;

import ${baseProjectPackage}.dao.${modelNameUpperCamel}Mapper;
import ${baseProjectPackage}.model.${modelNameUpperCamel};
import ${baseProjectPackage}.model.${modelNameUpperCamel}DetailInput;
import ${baseProjectPackage}.model.${modelNameUpperCamel}ListInput;
import ${baseProjectPackage}.service.${modelNameUpperCamel}Service;
import ${basePackage}.core.AbstractService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import com.github.pagehelper.PageInfo;
import com.github.pagehelper.PageHelper;

import java.util.List;


/**
 * Created by ${author} on ${date}.
 */
@Service
@Transactional(readOnly = true)
public class ${modelNameUpperCamel}ServiceImpl extends AbstractService<${modelNameUpperCamel}> implements ${modelNameUpperCamel}Service {
    @Resource
    private ${modelNameUpperCamel}Mapper ${modelNameLowerCamel}Mapper;

    @Override
    public ${modelNameUpperCamel} findDetailByInput(${modelNameUpperCamel}DetailInput input) {
        ${modelNameUpperCamel} cond = handleCondByParam(input);
        return ${modelNameLowerCamel}Mapper.selectOne(cond);
    }
    /**
    * 根据请求参数组装条件实体类
    * @param input
    * @return
    */
    private ${modelNameUpperCamel} handleCondByParam(${modelNameUpperCamel}DetailInput input) {
        ${modelNameUpperCamel} cond = new ${modelNameUpperCamel}();
        cond.setId(input.getId());
        if(StringUtils.isNotEmpty(input.getName())){
            cond.setUsername(input.getName());
        }
        return  cond;
    }
    @Override
    public PageInfo findListByInput(${modelNameUpperCamel}ListInput input) {
        PageHelper.startPage(input.getPageNum(), input.getPageSize());
        ${modelNameUpperCamel} cond = handleCondByParam(input);
        List<${modelNameUpperCamel}>  list = ${modelNameLowerCamel}Mapper.select(cond);
        PageInfo pageInfo = new PageInfo(list);
        return pageInfo;
    }

    /**
    * 根据请求参数组装条件实体类
    * @param input
    * @return
    */
    private User handleCondByParam(UserListInput input) {
        ${modelNameUpperCamel} cond = new ${modelNameUpperCamel}();
        cond.setId(input.getId());
        if(StringUtils.isNotEmpty(input.getName())){
            cond.setUsername(input.getName());
        }
        return  cond;
    }
}