package ${baseProjectPackage}.model;

import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.*;
import java.io.Serializable;

/**
* Created by ${author} on ${date}.
*/
public class ${modelNameUpperCamel}${bussNameUpperCamel}Input implements Serializable {
    @NotNull(message = "id不能为空")
    private  Integer id;
    @Length(max =10,message = "name字符串大小必须小于10")
    private  String name;

    public Integer getId() {
    return id;
    }

    public void setId(Integer id) {
    this.id = id;
    }

    public String getName() {
    return name;
    }

    public void setName(String name) {
    this.name = name;
    }
}
