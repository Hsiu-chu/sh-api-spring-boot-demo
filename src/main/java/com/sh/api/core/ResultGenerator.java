package com.sh.api.core;

import com.sh.api.core.Result;
import com.sh.api.core.ResultCode;

import java.util.List;

/**
 * 响应结果生成工具
 */
public class ResultGenerator {
    private static final String DEFAULT_SUCCESS_MESSAGE = "SUCCESS";
    private static final String DEFAULT_FAIL_MESSAGE = "FAIL";
    public  static Result genResult(ResultCode code,String message){
        return new Result()
                .setCode(code)
                .setMessage(message);
    }
    public  static Result genResult(int ret,String message){
        return new Result()
                .setRet(ret)
                .setMessage(message);
    }
    public static Result genSuccessResult() {
        return genResult(ResultCode.SUCCESS,DEFAULT_SUCCESS_MESSAGE);
    }
    public static <T> Result<T> genSuccessResult(T data) {
        return genResult(ResultCode.SUCCESS,DEFAULT_SUCCESS_MESSAGE)
                .setData(data);
    }
    public static <T> Result<T> genSuccessResult(List<T> list,long total) {
        return genResult(ResultCode.SUCCESS,DEFAULT_SUCCESS_MESSAGE)
                .setList(list)
                .setTotal(total);
    }
    public static Result genFailResult(String message) {
        return genResult(ResultCode.FAIL,message);
    }
    public static Result genFailResult() {
        return genResult(ResultCode.FAIL,DEFAULT_FAIL_MESSAGE);
    }
}
