package com.sh.api.core;

import com.alibaba.fastjson.JSON;

import java.util.List;

/**
 * 统一API响应结果封装
 */
public class Result<T> {
    private int ret;
    private String message;
    private T data;

    private List<T> list;
    private Long total;

    public Result() {
    }

    public Result setCode(ResultCode resultCode) {
        this.ret = resultCode.ret();
        return this;
    }
    public Result setRet(int ret) {
        this.ret = ret;
        return this;
    }

    public int getRet() {
        return ret;
    }

    public String getMessage() {
        return message;
    }

    public Result setMessage(String message) {
        this.message = message;
        return this;
    }

    public T getData() {
        return data;
    }

    public Result setData(T data) {
        this.data = data;
        return this;
    }
    public List<T> getList() { return list; }

    public Result setList(List<T> list) {
        this.list = list;
        return this;
    }

    public Long getTotal() { return total;}

    public Result setTotal(Long total) {
        this.total = total;
        return this;
    }

    @Override
    public String toString() {
        return JSON.toJSONString(this);
    }
}
