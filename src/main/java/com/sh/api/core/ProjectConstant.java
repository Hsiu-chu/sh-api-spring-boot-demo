package com.sh.api.core;

/**
 * 项目常量
 */
public final class ProjectConstant {
    public static final String BASE_PACKAGE = "com.sh.api";//代码所在的基础包名称，可根据自己公司的项目修改（注意：这个配置修改之后需要手工修改src目录项目默认的包路径，使其保持一致，不然会找不到类）
    public static final String RE_PACKAGE = ".*";
    public static final String SCAN_MODEL_PACKAGE = BASE_PACKAGE + RE_PACKAGE+ ".model";//package to scan for domain objects
    public static final String SCAN_MAPPER_PACKAGE = BASE_PACKAGE + RE_PACKAGE + ".dao";//生成的Mapper所在包
    public static final String MAPPER_INTERFACE_REFERENCE = BASE_PACKAGE + ".core.Mapper";//Mapper插件基础接口的完全限定名
}
