package com.sh.api.demo.model;

import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.Min;
import java.io.Serializable;

/**
* Created by ht_CodeGenerator on 2020/04/15.
*/
public class UserListInput implements Serializable {
    private  Integer id;
    @Length(max =10,message = "name字符串大小必须小于10")
    private  String name;
    @Min(value = 1,message="pageNum必须大于等于1")
    private int pageNum;
    @Min(value = 1,message="pageSize必须大于等于1")
    private int pageSize;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPageNum() {
        return pageNum;
    }

    public void setPageNum(int pageNum) {
        this.pageNum = pageNum;
    }
    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }
}
