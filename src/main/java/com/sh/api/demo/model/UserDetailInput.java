package com.sh.api.demo.model;

import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.*;
import java.io.Serializable;

/**
* Created by ht_CodeGenerator on 2020/04/14.
*/
public class UserDetailInput implements Serializable {
    @NotNull(message = "id不能为空")
    private  Integer id;
    @Length(max =10,message = "name字符串大小必须小于10")
    private  String name;

    public Integer getId() {
    return id;
    }

    public void setId(Integer id) {
    this.id = id;
    }

    public String getName() {
    return name;
    }

    public void setName(String name) {
    this.name = name;
    }
}
