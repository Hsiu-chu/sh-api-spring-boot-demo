package com.sh.api.demo.service.impl;

import com.github.pagehelper.PageHelper;
import com.sh.api.demo.dao.UserMapper;
import com.sh.api.demo.model.User;
import com.sh.api.demo.model.UserDetailInput;
import com.sh.api.demo.model.UserListInput;
import com.sh.api.demo.service.UserService;
import com.sh.api.core.AbstractService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import com.github.pagehelper.PageInfo;

import java.util.List;


/**
 * Created by ht_CodeGenerator on 2020/04/14.
 */
@Service
@Transactional(readOnly = true)
public class UserServiceImpl extends AbstractService<User> implements UserService {
    @Resource
    private UserMapper tUUserMapper;

    @Override
    public User findDetailByInput(UserDetailInput input) {
        User cond = handleCondByParam(input);
        return tUUserMapper.selectOne(cond);
    }
    /**
    * 根据请求参数组装条件实体类
    * @param input
    * @return
    */
    private User handleCondByParam(UserDetailInput input) {
        User cond = new User();
        cond.setId(input.getId());
        if(StringUtils.isNotEmpty(input.getName())){
            cond.setUsername(input.getName());
        }
        return  cond;
    }
    @Override
    public PageInfo findListByInput(UserListInput input) {
        PageHelper.startPage(input.getPageNum(), input.getPageSize());
        User cond = handleCondByParam(input);
        List<User>  list = tUUserMapper.select(cond);
        PageInfo pageInfo = new PageInfo(list);
        return pageInfo;
    }

    /**
    * 根据请求参数组装条件实体类
    * @param input
    * @return
    */
    private User handleCondByParam(UserListInput input) {
        User cond = new User();
        cond.setId(input.getId());
        if(StringUtils.isNotEmpty(input.getName())){
            cond.setUsername(input.getName());
        }
        return  cond;
    }
}