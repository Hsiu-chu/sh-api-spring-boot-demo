package com.sh.api.demo.dao;

import com.sh.api.core.Mapper;
import com.sh.api.demo.model.User;

public interface UserMapper extends Mapper<User> {
}